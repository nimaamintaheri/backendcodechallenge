## Backend Coding Challenge 

In order to run the project you need to do the folowing.

### Prerequisites

- php
- symfony
- sqlite php driver or you can configure any other database.

## Instructions

```
git clone https://nimaamintaheri@bitbucket.org/nimaamintaheri/backendcodechallenge.git
cd backendcodechallenge
composer install
php bin/console doctrine:migrations:migrate
php bin/console app:seed
symfony server:start
```

I wrote a command to feed the database from the github api. It's name is app:seed.
The address for the table of commits is http://127.0.0.1:8000/commits