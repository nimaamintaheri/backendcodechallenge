<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Commit;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\HttpClientClass;

class SeedCommand extends Command
{
    protected static $defaultName = 'app:seed';
    protected static $defaultDescription = 'Adds 25 most recent commits from https://github.com/nodejs/node to the database.';
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    private $client;
    public function __construct( EntityManagerInterface $manager, HttpClientClass $client) {
        $this->manager = $manager;
        $this->client = $client;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        $collection = $this->client->getLastCommits();
        $count = 0;

        foreach($collection as $commit) {
            try {
                $this->manager->persist($commit);
                $this->manager->flush();
                $count++;
                if ($count > 25) break;
            }
            catch(\Throwable $e) {
                continue;
            }
        }

        $io->success('Commits('.$count.') are inserted successfully.');

        return Command::SUCCESS;
    }

}
