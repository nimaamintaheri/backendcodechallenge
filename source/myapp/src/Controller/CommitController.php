<?php

namespace App\Controller;

use App\Repository\CommitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\HttpClientClass;
class CommitController extends AbstractController
{
    /**
     * @Route("/commits", name="commit")
     */
    public function index(CommitRepository $commitRepository): Response
    {
        return $this->render('commit/index.html.twig', [
            'controller_name' => 'CommitController',
            'commits' => $commitRepository->findAll()
        ]);
    }

}
