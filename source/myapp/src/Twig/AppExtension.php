<?php

// src/Twig/AppExtension.php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('endsInNumber', [$this, 'IsEndInNumber']),
        ];
    }

    public function IsEndInNumber(string $str)
    {
        return is_numeric(substr($str, -1, 1));
    }
}