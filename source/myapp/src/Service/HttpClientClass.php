<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Commit;

class HttpClientClass
{
    private $manager;
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    private function fetchGitHubCommits(): array
    {
        $response = $this->client->request(
            'GET',
            'https://api.github.com/repos/nodejs/node/commits'
        );

        return $response->toArray();
    }

    private function convertToCommitObject($arr) {
        $collection = new ArrayCollection($arr);
        return $collection->map(function($value) {
            $commit = new Commit;

            $commit->setSha($value["sha"]);
            $commit->setAuthorName($value["commit"]["author"]["name"]);
            $commit->setAuthorEmail($value["commit"]["author"]["email"]);
            $commit->setMessage($value["commit"]["message"]);
            $commit->setUrl($value["url"]);

            return $commit;
        });
    }

    public function getLastCommits() {
        return $this->convertToCommitObject($this->fetchGitHubCommits());
    }
}